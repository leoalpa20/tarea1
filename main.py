#!/usr/bin/env python
# -- coding: utf-8 --

#Estructuras de Computadores digitales II
#Tarea 1
#Leonardo Alfaro Patino
#main.py


from sys import argv
from twoBitBranchPredictor import bimodal_predictor
from privateHistoryBranchPredictor import private_history_branch_predictor
from globalHistoryBranchPredictor import global_history_predictor
from tournamentBranchPredictor import tournament_predictor

scrip, arg1,arg2,arg3,arg4=argv

s=int(arg1)
bp=int(arg2)
gh=int(arg3)
ph=int(arg4)

print("Para correr los otros contadores, descomentarlos del archivo main y correr uno por uno")

#tournament_predictor(s, bp)
bimodal_predictor(s)
#global_history_predictor(s, bp)
#private_history_branch_predictor(s, bp)


